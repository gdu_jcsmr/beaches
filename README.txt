This file reports the problem samples that were discarded during processing

1.1.1.34 didn't decode any reads
3.2.1.34 didn't decode any reads 

w1_1_2_16 couldn't form any stacks due to insufficient data
w2_2_2_33
w3_1_2_17
w2_2_1_16

The following samples were removed by cstacks

w2_2_2_35

The following samples failed at the tsv2bam stage

w2_1_1_5
w2_1_2_3
w2_1_2_39
w2_2_2_3
w3_1_1_58
w3_1_2_9

Removed 93152 loci that did not pass sample/population constraints from 95126 loci.
Kept 1974 loci, composed of 274534 sites; 7779 of those sites were filtered, 1347 variant sites remained.
Number of loci with PE contig: 1962.00 (99.4%);
  Mean length of loci: 129.51bp (stderr 0.22);
Number of loci with SE/PE overlap: 108.00 (5.5%);
  Mean length of overlapping loci: 110.94bp (stderr 0.37); mean overlap: 19.91bp (stderr 0.34);
Mean genotyped sites per locus: 129.61bp (stderr 0.21).

Population summary statistics (more detail in populations.sumstats_summary.tsv):
  1: 11.278 samples per locus; pi: 0.404; all/variant/polymorphic sites: 43347/320/302; private alleles: 2
  2: 10.07 samples per locus; pi: 0.4633; all/variant/polymorphic sites: 67421/373/370; private alleles: 0
  3: 10.938 samples per locus; pi: 0.45582; all/variant/polymorphic sites: 233754/1191/1185; private alleles: 5
  4: 10.983 samples per locus; pi: 0.44957; all/variant/polymorphic sites: 233723/1127/1106; private alleles: 6
  5: 9.303 samples per locus; pi: 0.44789; all/variant/polymorphic sites: 12155/33/33; private alleles: 0
  6: 10.346 samples per locus; pi: 0.43266; all/variant/polymorphic sites: 79669/410/399; private alleles: 0
  7: 8.8271 samples per locus; pi: 0.45623; all/variant/polymorphic sites: 48264/133/131; private alleles: 0
  8: 10.894 samples per locus; pi: 0.42746; all/variant/polymorphic sites: 128581/630/615; private alleles: 0
  9: 10.975 samples per locus; pi: 0.42723; all/variant/polymorphic sites: 47026/317/310; private alleles: 0
  10: 10.555 samples per locus; pi: 0.39534; all/variant/polymorphic sites: 31120/218/209; private alleles: 0
  11: 9.273 samples per locus; pi: 0.45607; all/variant/polymorphic sites: 93715/359/353; private alleles: 0
  12: 10.286 samples per locus; pi: 0.42756; all/variant/polymorphic sites: 13606/42/40; private alleles: 0
Populations is done.

Reading ustacks_worms/populations.var.phylip
467 samples and 1319 loci found
Creating filtered PHYLIP file worms_filt05_p2_20perc_maf02.phy with 256 samples, and 1279 bases

