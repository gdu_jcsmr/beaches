#!/bin/bash

######
# Process GBS data de novo with Stacks v2.0
# 1. Run the section from USTACKS to GSTACKS
# 2. Iterate through POPULATIONS and INDIVIDUALS with settings that keep most
# data points while clearly removing minimally informative or unreliable SNPs
# 3. The IQ-tree step should be run last on its own and includes a step to 
# remove uninformed samples (default <5% of informative SNP sites) 
#
# This script is built for a cluster running SGE. Simply strip out the qsub 
# content to run locally. Total processing time on a modern machine should 
# be in the order of 18 hours.
#
# All scripts and analysis performed by Cameron Jack, 2017.
# ANU Bioinformatics Consultancy, cameron.jack@anu.edu.au
######

# run axe-demux with barcode file on reads in raw, sving to folder demux
# trimit -l 64 -L 64 -o clean/w3_2_2_7.fastq demux/worms_3.2.2.7_il.fastq
# for j in *.fastq; do deinterleave_fastq.sh < $j $(basename $j .fastq).1.fq $(basename $j .fastq).2.fq [compress]; done
# for f in *.fq; do gzip $f; done
# rm *.fastq 


if [ ! -d ustacks_worms ] ; then
    mkdir -p ustacks_worms
fi

FIX_HEADERS=0
if [ $FIX_HEADERS == 1 ] ; then
    cp fix_headers.sh clean/.
    cd clean
    ./fix_headers.sh
    cd ..
    #cd ustacks_worms
    #for f in *.tags.tsv.gz; do ../fix_tags.py $f; done
    #cd ..
fi

USTACKS=0
if [ $USTACKS == 1 ] ; then
    count=1
    for f in clean/*.1.fq.gz
    do 
        fx=`basename ${f/.1.fq.gz/}`
        qsub -pe threads 6 -cwd -N 'ustacks' -b y \
            ustacks -f $f -o ustacks_worms --name $fx -i $count -m 5 -M 3 -d -p 6
        count=$((count + 1))
    done
fi

CSTACKS=0
if [ $CSTACKS == 1 ] ; then
    qsub -pe threads 4 -cwd -hold_jid 'ustacks' -N 'cstacks' -m e \
        -M 'cameron.jack@anu.edu.au' -b y \
        -l h_vmem=3.2g,virtual_free=3.1g \
        cstacks -P ustacks_worms -p 4 -n 4 -M naive.map 
fi

SSTACKS=0
if [ $SSTACKS == 1 ] ; then
    qsub -pe threads 4 -cwd -hold_jid 'cstacks' -N 'sstacks' -m e \
        -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g \
        sstacks -P ustacks_worms -p 4 -M naive.map
fi


TSV2BAM=0
if [ $TSV2BAM == 1 ] ; then
    qsub -pe threads 4 -cwd -hold_jid 'sstacks' -N 'tsv2bam' -m e \
        -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g \
        tsv2bam -P ustacks_worms -t 4 -M naive.map -R clean
fi

GSTACKS=0
if [ $GSTACKS == 1 ] ; then
    qsub -pe threads 4 -cwd -hold_jid 'tsv2bam' -N 'gstacks' -m e \
        -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g \
        gstacks -P ustacks_worms -t 4 -M naive.map --min_kmer_cov 10
fi

POPULATIONS=0
if [ $POPULATIONS == 1 ] ; then
    # Population level... there are 12 pop groups
    # run this first to filter loci, then remap the samples to separate population groups and run again
    qsub -pe threads 12 -cwd -hold_jid 'gstacks' -N 'populations' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g populations -P ustacks_worms -t 12 -M naive.map \
        -p 2 -r 0.1 --min_maf 0.2 --phylip_var --lnl_lim -20
    qsub -cwd -N 'whitelist' -hold_jid 'populations' -b y \
        python make_whitelist.py --sumstats ustacks_worms/populations.sumstats.tsv \
        --whitelist worms_p2_10perc_maf02_mincov10_lnl-20.whitelist
fi

INDIVIDUALS=0
if [ $INDIVIDUALS == 1 ] ; then
    qsub -pe threads 12 -cwd -N 'individuals' -m e \
        -M 'cameron.jack@anu.edu.au' -l h_vmem=3g,virtual_free=2.9g \
        -hold_jid 'whitelist' -b y \
        populations -P ustacks_worms -t 12 -M indivs.map \
        --phylip_var --structure -W worms_p2_10perc_maf02_mincov10_lnl-20.whitelist \
        --batch_size 1000
fi

### Run IQTREE seperately, after everything else has finished
IQTREE=1
if [ $IQTREE == 1 ] ; then
    qsub -cwd -V -N 'filter' -hold_jid 'individuals' -b y \
        "python filter_samples.py \
        --phy ustacks_worms/populations.var.phylip \
        --keep worms_filt05_p2_10perc_maf02_mincov10_lnl-20.fasta --cutoff 0.05 --fasta"
    qsub -pe threads 12 -cwd -N 'iqtree' -m e -M 'cameron.jack@anu.edu.au' -b y\
       	-l h_vmem=3g,virtual_free=2.9g -hold_jid 'filter' iqtree -nt 12 \
        -s worms_filt05_p2_10perc_maf02_mincov10_lnl-20.fasta -st DNA -m MFP -bb 5000 \
        -pre worms_filt05_p2_10perc_maf02_mincov10_lnl-20.fasta -bnni -nm 3000 -nstep 250 -redo

fi
