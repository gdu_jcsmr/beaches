#!/usr/bin/env python3

import os
import gzip
import argparse


def fix_read_headers(input, out):
    for i, line in enumerate(input):
        #print(line)
        if i % 4 == 0:
            cols = line.split(' ')
            if cols[1].startswith('2'):
                cols[0] += '/2\n'
            else:
                cols[0] += '/1\n'
            out.write(cols[0])
        else:
            out.write(line)


def main():
    """ 
        Illumina read headers from Casava 1.8 aren't often read properly
        by bioinformatics software. This program converts read headers 
        of the form 1326432:12323123:312312 1:N:123123 to
        112321321:2132132321:123213/1
        1) Rename the input file to xxx_filename
        2) Read the input file, modifying headers and write to 
        the original filename.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='input FASTQ file, can be gzipped')
    args = parser.parse_args()
    
    orig = args.input
    temp = 'xxx_' + args.input
    
    os.rename(orig, temp)

    if '.gz' in temp:
        with gzip.open(temp, 'rt') as input, gzip.open(orig, 'wt') as out:
             fix_read_headers(input, out)
    else:
        with open(temp, 'r') as input, open(orig, 'w') as out:
             fix_read_headers(input, out)

    os.remove(temp)        

if __name__ == '__main__':
    main()
