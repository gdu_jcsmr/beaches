#!/usr/bin/env python

import sys
import argparse
import numpy
from matplotlib import pyplot as plt
from math import log10

"""
    Name: filter_samples.py
    Author: Cameron Jack, 2018, ANU Bioinformatics Consultancy
    Contact: cameron.jack@anu.edu.au
    Performs one main task: removing uninformed samples from phylo/
    population genetics analysis. Uninformed samples with less than 
    say, 5% informative SNP sites may end clustering next to just about 
    any other sample through random chance. This denies the clustering
    algorithm a chance to cluster another sample with a more meaningful
    alternative. Since there is no option for this in STACKS, I wrote this
    script to do it for me. Once filtered, it then removes SNP sites that
    are now uninformative (not used in any or 1 sample - singletons tell 
    us nothing) and looks again. It iterates this process until no 
    uninformed samples and no uninformative SNP sites exist.

    Earlier versions of this code included some histogram plots that were
    useful in helping to justify the 5% cutoff, but it's quick to try 
    different values and simply look where thee "knee" cutoff lies. The 
    default cutoff of 5% should work fine unless you have very few 
    loci, or hugely many, and these have a very odd coverage distribution.

    There is no license for this code. Do with it as you will.
"""


def read_phy(args):
    """
        Read the input .phy file. Each line becomes a tuple of sample and loci
        Return list of tuples, number of samples, number of loci
    """
    sample_loci = []
    with open(args.phy, 'r') as f:
        for i, line in enumerate(f):
            if line.startswith('#'):
                continue  # comment
            if i == 0:
                continue  # header
            if '\t' in line:
                sample, nucs = line.split('\t')
            elif ' ' in line.strip():
                sample, nucs = line.split()
            else:                
                sample = line[0:10]
                nucs = line.strip()[10:]
            sample_loci.append(tuple([sample, nucs]))
    return sample_loci, i-1, len(nucs)


def remove_uninformed_samples(args, sample_loci):
    """
        Uninformed samples are a major problem as they can end up appearing 
        closely related to many potentially unrelated samples.
    """
    kept_sample_loci = []
    discarded = 0
    for sample, nucs in sample_loci:
        total_positions = len(nucs)
        count_a = nucs.count('a') + nucs.count('A')
        count_c = nucs.count('c') + nucs.count('C')
        count_g = nucs.count('g') + nucs.count('G')
        count_t = nucs.count('t') + nucs.count('T')
        count_r = nucs.count('r') + nucs.count('R')
        count_y = nucs.count('y') + nucs.count('Y')
        count_s = nucs.count('s') + nucs.count('S')
        count_w = nucs.count('w') + nucs.count('W')
        count_k = nucs.count('k') + nucs.count('K')
        count_m = nucs.count('m') + nucs.count('M')
        count_n = nucs.count('n') + nucs.count('N')
        count_homs = count_a + count_c + count_g + count_t
        count_hets = count_r + count_y + count_s + count_w +\
                     count_k + count_m
        count_other = total_positions - count_homs - count_hets - count_n
        info_prop = (count_homs + count_hets + count_other) / total_positions
        if info_prop > args.cutoff:
            kept_sample_loci.append(tuple([sample, nucs]))
            print('Keeping', sample, info_prop)
        else:
            print('Discarding', sample, info_prop)
            discarded += 1
    return kept_sample_loci, discarded


def remove_uninformative_sites(args, sample_loci):
    """
        After removing samples, we may create a situation where now some 
        sites are no longer variable or are only variable in one sample 
        (singletons). The constant loci must be removed or IQ-Tree won't 
        run, and removing singletons should improve run speed.
    """
    variant_loci_indices = set()
    num_invariant = 0
    sample, nucs = sample_loci[0]
    num_loci = len(nucs)
    print ('... evaluating loci for invariant/common sites')
    for i in range(num_loci):
        # get all bases at this locus, ignore Ns
        locus_bases = set()
        for sample, nucs in sample_loci:
            if (nucs[i].lower()) != 'n':
                locus_bases.add(nucs[i].lower())
        # keep this locus if we have more than one base
        if len(locus_bases) > 1:
            if args.noambig:
                ambig_bases = set(['r','y','w','s','k','m'])
                overlap = ambig_bases.intersection(locus_bases)
                if len(overlap) == 0:
                    variant_loci_indices.add(i)
                else:
                    num_invariant += 1
            else:
                variant_loci_indices.add(i)            
        else:
            num_invariant += 1

    print('... building new sample loci table')
    kept_sample_loci = []
    for sample, nucs in sample_loci:
        kept_nucs = []
        for i, locus in enumerate(nucs):
            if i in variant_loci_indices:
                kept_nucs.append(locus)
        kept_sample_loci.append(tuple([sample, ''.join(kept_nucs)]))

    return kept_sample_loci, num_invariant
    

def write_phy(args, sample_loci):
    """
        Write sample names and SNP info back to PHYLIP format.
        This format has 10 fixed characters for sample name, 
        padded with blank spaces, followed hard by the SNP bases.
    """
    with open(args.keep, 'w') as out:
        # calculate header first
        num_samples = len(sample_loci)
        num_bases = len(sample_loci[0][1])
        out.write(str(num_samples) + '    ' + str(num_bases) + '\n')
        # now write out the entries
        for sample, loci in sample_loci:
            out.write(sample + loci + '\n')


def write_fasta(args, sample_loci):
    """
        Write sample names and SNP info back to FASTA format.
    """
    with open(args.keep, 'w') as out:
        for sample, loci in sample_loci:
            out.write('>' + sample + '\n')
            out.write(loci + '\n')


def main():
    """
        Remove samples with less than --cutoff proportion of informative loci
        There may now exist a number of sites that are invariant, remove these
        Repeat this process till there is no further change
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--phy', required=True, help='Path to input phylip file')
    parser.add_argument('--keep', required=True, help='Path to output phylip file')
    parser.add_argument('--cutoff', type=float, default=0.05, 
            help='Min informative SNP proportion per sample, default=0.05')
    parser.add_argument('--plots', action='store_true', 
            help='Enable information content histograms - CURRENTLY DISABLED')
    parser.add_argument('--noambig', action='store_true', help='Ambiguous base '+\
            'codes fail with some phylogenetic models. This removes loci '+\
            'containing ambiguous base codes')
    parser.add_argument('--fasta', action='store_true', help='Save in FASTA format')
    args = parser.parse_args()

    print('Reading', args.phy)
    sample_loci, pre_samples, pre_loci = read_phy(args)
    print(pre_samples, 'samples and', pre_loci, 'loci found')

    bad_samples = 1  # this is required as there is no do..while loop in python
    invariant_loci = 1  # as above
    # this loop always converges to 0
    while bad_samples > 0 or invariant_loci > 0:
        sample_loci, bad_samples = remove_uninformed_samples(args, sample_loci)
        print('Removed', bad_samples, 'uninformed samples')       
        sample_loci, invariant_loci = remove_uninformative_sites(args, sample_loci)
        print('Removed', invariant_loci, 'invariant loci')

    post_samples = len(sample_loci)
    post_loci = 0
    if post_samples > 0:
        post_loci = len(sample_loci[0][1])
    print('Creating filtered PHYLIP file', args.keep, 'with', post_samples,
          'samples, and', post_loci, 'bases')
    if args.fasta:
        write_fasta(args, sample_loci)
    else:
        write_phy(args, sample_loci)


if __name__ == '__main__':
    main()
